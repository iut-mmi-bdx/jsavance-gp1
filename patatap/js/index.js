/* 
Preloader
*/
const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
const numberHTML = document.getElementById('number')
const loader = document.getElementById('loader')
let number = 0

for (let i = 0; i < alphabet.length; i++) {

    const letter = alphabet[i]
    const url = `sounds/${letter}.mp3` 
    const audio = new Audio(url)

    audio.addEventListener(
        'canplaythrough',
        () => {
            number++
            numberHTML.textContent = number

            if (number === 26) loader.classList.add('hide')
        }
    )
}





/* 
Récupère le nom de notre utilisateur,
& on l'affiche dans le DOM
*/
const getName = () => {

    let name = localStorage.getItem('name')

    if (!name) {
        name = prompt(`Comment t'appelles-tu ?`)
        localStorage.setItem('name', name)
    }
    
    const span = document.getElementById('name')
    span.textContent = `Bonjour ${name}`
}

getName()

/* 
Écoute notre DOM à chque touche du clavier appuyée,
& on affiche la valeur de la touche si c'est une lettre
*/
window.addEventListener(
    'keyup', 
    (event) => {

        const isLetter = alphabet.includes(event.key);
        if (isLetter) {
            playSound(event.key)
            animationLetter.push(event.key)
        }
    }
)

const playSound = (letter) => {
    const url = `sounds/${letter}.mp3` 
    const audio = new Audio(url)
    audio.play()

    audio.addEventListener(
        'ended',
        () => {
            const index = animationLetter.indexOf(letter)
            animationLetter.splice(index, 1)
        }
    )
}

/* 
P5.js
Ajout d'un espace de dessin
*/
const width = window.innerWidth
const height = window.innerHeight

let animationLetter = []
let colors = [
    [255, 0, 0],
    [0, 255, 0]
]

function setup() {
    const canvas = createCanvas(width, height)
    canvas.parent('canvasContainer')
}

function draw() {
    clear()
    background('rgba(0, 0, 0, 0)')
    if ( animationLetter.includes('a') ) shapeA()
    if ( animationLetter.includes('b') ) shapeB()
}

const shapeA = () => {
    const r = colors[0][0]
    const g = colors[0][1]
    const b = colors[0][2]
    fill(r, g, b)
    ellipse(200, 200, 100)
}

const shapeB = () => {
    const r = colors[1][0]
    const g = colors[1][1]
    const b = colors[1][2]
    fill(r, g, b)
    ellipse(width / 2, height / 2, 100)
}

/*
Fonction pour afficher et cacher notre liste d'images
*/

const button = document.getElementById('colorsButton')
const list = document.getElementById('colorsContainer')

button.addEventListener(
    'click',
    () => {
        list.classList.toggle('hide')
    }
)

/*
1 - OK ouvert un liste
2 - OK clic sur une image
3 - OK changer la couleur de nos formes
*/

const images = document.querySelectorAll('.patatap__image')

for (let i = 0; i < images.length; i++ ){
    const image = images[i]

    image.addEventListener(
        'click',
        () => {
            const src = image.getAttribute('src')
            const vibrant = new Vibrant(src)
            vibrant.getPalette( (err, palette) => {
                colors = []
                if ( palette.DarkMuted )    colors.push(palette.DarkMuted._rgb)
                if ( palette.DarkVibrant )  colors.push(palette.DarkVibrant._rgb)
                if ( palette.LightMuted )   colors.push(palette.LightMuted._rgb)
                if ( palette.LightVibrant ) colors.push(palette.LightVibrant._rgb)
                if ( palette.Muted )        colors.push(palette.Muted._rgb)
                if ( palette.Vibrant )      colors.push(palette.Vibrant._rgb)
                button.click()
            })
        }
    )
}