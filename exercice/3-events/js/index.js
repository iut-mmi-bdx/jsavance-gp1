const button = document.getElementById('button')
const p = document.getElementById('countdown')

let number = 0
p.textContent = number

button.addEventListener(
    'click',
    () => {
        //console.log('clic')
        number++
        p.textContent = number
    }
)
/*

qui.addEventListener(
    'quel évènement',
    () => {
        // qu'est ce que je fais quand l'évènement est déclenché
    }
)

*/