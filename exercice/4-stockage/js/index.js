// Session Storage
//sessionStorage.setItem('name', 'Charline')
const name = sessionStorage.getItem('name')
console.log('name :', name)


// Local Storage
//localStorage.setItem('lastname', 'Laporte')
const lastname = localStorage.getItem('lastname')
console.log('lastname :', lastname)