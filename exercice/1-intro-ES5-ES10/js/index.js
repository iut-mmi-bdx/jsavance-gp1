/*
ES5
Rappel variables, fonctions
*/
var string = 'chaine de caractères'
var number = 1
var array = ['1', 2, 3]
var object = {
    name: 'Charline',
    lastname: 'Nom de famille'
}
var bool = false

function nom(params) {
    // ce qu'on doit faire
}

/*
ES6
Comment on écrit les variables et les fonctions avec la nouvelle notation
*/

// 1 - variables
// si la variable est modifiable, on utilise le mot-clé "let"
let age = 27
age++

// si la variable est non-modifiable, on utilise le mot-clé "const"
const name = 'Charline'
//name = 'Charles' // ce code produit une erreur, il n'est pas possible de modifier une variable déclarée avec "const"


// 2 - fonctions
const myFunction = (params) => {
    console.log('mes paramètres sont', params)
}


// Exercice

/* 
Écrire une fonction qui prend en paramètre un tableau. 
Le tableau est composé en premier d’un nombre, votre âge, 
puis un objet avec propriétés nom / prénom
La fonction fonction retourne « Bonjour je m’appelle x y et j’ai x ans »
Console.log le résultat de la fonction
*/

// 1 - Fonction : quand on lui un tableau, ici appelé params 
// il affiche une phrase avec les valeur de ce tableau
const returnString = (params) => {
    const age = params[0]
    const firstname = params[1].firstname
    const lastname = params[1].lastname

    const string = `Bonjour je m'appelle ${firstname} ${lastname} et j'ai ${age} ans`
    console.log(string)
}

const myInfosName = {
    firstname: 'Charline', 
    lastname : 'Laporte'
}
const charline = [27, myInfosName]   
//returnString(charline)

const myInfosNameCharles = {
    firstname: 'Charles', 
    lastname : 'Laporte'
}
const charles = [19, myInfosNameCharles]   
//returnString(charles)

const charli = [18, myInfosName]  
//returnString(charli)

const addition = (a, b) => {
    console.log(a + b)
}
//addition(1, 2)
//addition(2, 5)



const sayHello = () => {
    // programme
    console.log('bonjour')
}

sayHello()

const saySomething = (something) => {
    // programme
    console.log(something)
}

saySomething('Comment ça va ?')
saySomething('Au revoir')